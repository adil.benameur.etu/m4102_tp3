package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.*;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());

    /**
     * Pizza de base :
     *   {
     *     "id": "59f51c59-7144-42e1-9e5d-a908dc7dc4eb",
     *     "name": "margarita",
     *     "priceSmall": 8.5,
     *     "priceLarge": 10.5,
     *     "ingredients": [{ "id": "f38806a8-7c85-49ef-980c-149dcd81d306", "name": "mozzarella"}, { "id": "c77deeee-d50d-49d5-9695-c98ec811f762", "name": "tomate"}, { "id": "bc5b315f-442f-4ee4-96de-486d48f20c2f", "name": "champignons"}]
     *   }
     */

    private IngredientDao ingredientDao;
    private PizzaDao pizzaDao;

    private Pizza pizzaBase;

    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        return new ApiV1();
    }

    @Before
    public void setEnvUp() {
        ingredientDao = BDDFactory.buildDao(IngredientDao.class);
        ingredientDao.createTable();

        ingredientDao.insert(new Ingredient(UUID.fromString("657f8dd4-6bc1-4622-9af7-37d248846a23"), "fromage"));

        pizzaDao = BDDFactory.buildDao(PizzaDao.class);
        pizzaDao.createTableAndIngredientAssociation();

        pizzaBase = new Pizza(UUID.randomUUID(), "Saumon", 0.0, 0.0, List.of(new Ingredient(UUID.fromString("657f8dd4-6bc1-4622-9af7-37d248846a23"), "fromage")));
        pizzaDao.insert(pizzaBase);
    }

    @After
    public void tearEnvDown() {
        pizzaDao.dropTableAndIngredientAssociation();
        ingredientDao.dropTable();
    }

    @Test
    public void testGetExistingPizza() {
        Response response = target("/pizzas").path(pizzaBase.getId().toString()).request(MediaType.APPLICATION_JSON).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(pizzaBase, result);
    }

    @Test
    public void testGetNotExistingPizza() {
        Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }

    @Test
    public void testCreatePizza() {
        Pizza newPizza = new Pizza(UUID.randomUUID(), "pizza de ouf", 0.0, 0.0, List.of());
        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(newPizza);
        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
        assertEquals(returnedEntity.getPriceSmall(), pizzaCreateDto.getPriceSmall(), 5);
        assertEquals(returnedEntity.getPriceLarge(), pizzaCreateDto.getPriceLarge(), 5);
    }

    @Test
    public void testCreateSamePizza() {
        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizzaBase);
        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }

    @Test
    public void testDeleteExistingPizza() {
        Response response = target("/pizzas/").path(pizzaBase.getId().toString()).request().delete();

        assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

        Pizza result = pizzaDao.findById(pizzaBase.getId());
        assertEquals(result, null);
    }

    @Test
    public void testDeleteNotExistingPizza() {
        Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testGetNotExistingPizzaName() {
        Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateWithForm() {
        Form form = new Form();
        form.param("name", "Peperoni");

        Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        Response response = target("pizzas").request().post(formEntity);

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        String location = response.getHeaderString("Location");
        String id = location.substring(location.lastIndexOf('/') + 1);
        Pizza result = pizzaDao.findById(UUID.fromString(id));

        assertNotNull(result);
    }
}
