package fr.ulille.iut.pizzaland.dao;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;
import java.util.UUID;

public interface PizzaDao {
    IngredientDao ingredients = BDDFactory.buildDao(IngredientDao.class);

    @Transaction
    default void createTableAndIngredientAssociation() {
        createPizzaTable();
        createAssociationTable();
    }

    @SqlUpdate("CREATE TABLE IF NOT EXISTS ingredients_pizza (pizza_id VARCHAR NOT NULL, ingredient_id VARCHAR NOT NULL, CONSTRAINT FK_pizza_id FOREIGN KEY(pizza_id) REFERENCES pizzas(id), CONSTRAINT FK_ingredient_id FOREIGN KEY(ingredient_id) REFERENCES ingredients(id), CONSTRAINT UQ_pizza_ingredient_id UNIQUE(pizza_id, ingredient_id))")
    void createAssociationTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL, price_small FLOAT, price_large FLOAT)")
    void createPizzaTable();

    @Transaction
    default void dropTableAndIngredientAssociation() {
        dropTableAssociationTable();
        dropTablePizzas();
    }

    @SqlUpdate("DROP TABLE IF EXISTS ingredients_pizza")
    void dropTableAssociationTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropTablePizzas();

    @Transaction
    default void insert(Pizza pizza) {
        insertPizza(pizza);
        for (Ingredient ingredient: pizza.getIngredients())
            if(ingredient.getId() == null)
                insertIngredients(pizza.getId(), ingredients.findByName(ingredient.getName()).getId());
            else
                insertIngredients(pizza.getId(), ingredient.getId());
    }

    @SqlUpdate("INSERT INTO pizzas (id, name, price_small, price_large) VALUES (:id, :name, :priceSmall, :priceLarge)")
    void insertPizza(@BindBean Pizza pizza);

    @SqlUpdate("INSERT INTO ingredients_pizza (pizza_id, ingredient_id) VALUES (:pizzaId, :ingredientId)")
    void insertIngredients(@Bind("pizzaId") UUID pizzaId, @Bind("ingredientId") UUID ingredientId);

    default void remove(UUID id) {
        removeIngredientsPizza(id);
        removePizza(id);
    }

    @SqlUpdate("DELETE FROM ingredients_pizza WHERE pizza_id = :id")
    void removeIngredientsPizza(@Bind("id") UUID id);

    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void removePizza(@Bind("id") UUID id);

    default Pizza findByName(String name) {
        Pizza p = findPizzaByName(name);
        if(p != null)
            p.setIngredients(getPizzaIngredient(p));
        return p;
    }

    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findPizzaByName(@Bind("name") String name);

    default List<Pizza> getAll() {
        List<Pizza> pizzas = getAllPizza();
        for (Pizza p : pizzas) {
            p.setIngredients(getPizzaIngredient(p));
        }
        return pizzas;
    }

    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAllPizza();

    default Pizza findById(UUID id) {
        Pizza p = findPizzaById(id);
        if(p != null)
            p.setIngredients(getPizzaIngredient(p));
        return p;
    }

    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findPizzaById(@Bind("id") UUID id);

    @SqlQuery("SELECT ingredients.id, ingredients.name FROM pizzas, ingredients_pizza, ingredients WHERE pizzas.id = pizza_id and pizzas.id = :id and ingredients.id = ingredients_pizza.ingredient_id;")
    @RegisterBeanMapper(Ingredient.class)
    List<Ingredient> getPizzaIngredient(@BindBean Pizza pizza);
}

