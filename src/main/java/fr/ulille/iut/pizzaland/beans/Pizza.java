package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class Pizza {
    private UUID id = UUID.randomUUID();
    private String name;
    private double priceSmall;
    private double priceLarge;
    private List<Ingredient> ingredients;

    public Pizza() {
    }

    public Pizza(UUID id, String name, double priceSmall, double priceLarge, List<Ingredient> ingredients) {
        this.id = id;
        this.name = name;
        this.priceSmall = priceSmall;
        this.priceLarge = priceLarge;
        this.ingredients = ingredients;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPriceSmall() {
        return priceSmall;
    }

    public void setPriceSmall(double priceSmall) {
        this.priceSmall = priceSmall;
    }

    public double getPriceLarge() {
        return priceLarge;
    }

    public void setPriceLarge(double priceLarge) {
        this.priceLarge = priceLarge;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredients(Ingredient ingredient) {
        this.ingredients.add(ingredient);
    }

    public static PizzaDto toDto(Pizza p) {
        PizzaDto dto = new PizzaDto();
        dto.setId(p.getId());
        dto.setName(p.getName());
        dto.setPriceLarge(p.getPriceLarge());
        dto.setPriceSmall(p.getPriceSmall());
        dto.setIngredients(p.getIngredients().parallelStream().map(Ingredient::toDto).collect(Collectors.toList()));
        return dto;
    }

    public static Pizza fromDto(PizzaDto dto) {
        Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());
        pizza.setPriceSmall(dto.getPriceSmall());
        pizza.setPriceLarge(dto.getPriceLarge());
        pizza.setIngredients(dto.getIngredients().parallelStream().map(Ingredient::fromDto).collect(Collectors.toList()));
        return pizza;
    }

    public static PizzaCreateDto toCreateDto(Pizza pizza) {
        PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
        dto.setPriceSmall(pizza.getPriceSmall());
        dto.setPriceLarge(pizza.getPriceLarge());
        dto.setIngredientCreateDto(pizza.getIngredients().parallelStream().map(Ingredient::toCreateDto).collect(Collectors.toList()));
        return dto;
    }

    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
        Pizza pizza = new Pizza();
        pizza.setName(dto.getName());
        pizza.setPriceSmall(dto.getPriceSmall());
        pizza.setPriceLarge(dto.getPriceLarge());
        pizza.setIngredients(dto.getIngredientCreateDto().parallelStream().map(Ingredient::fromIngredientCreateDto).collect(Collectors.toList()));
        return pizza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return Double.compare(pizza.priceSmall, priceSmall) == 0 &&
                Double.compare(pizza.priceLarge, priceLarge) == 0 &&
                Objects.equals(id, pizza.id) &&
                Objects.equals(name, pizza.name) &&
                Objects.equals(ingredients, pizza.ingredients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, priceSmall, priceLarge, ingredients);
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", priceSmall=" + priceSmall +
                ", priceLarge=" + priceLarge +
                ", ingredients=" + ingredients +
                '}';
    }
}
