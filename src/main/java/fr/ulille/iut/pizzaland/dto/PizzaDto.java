package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

public class PizzaDto {
    private UUID id = UUID.randomUUID();
    private String name;
    private double priceSmall;
    private double priceLarge;
    private List<IngredientDto> ingredients;

    public PizzaDto() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPriceSmall() {
        return priceSmall;
    }

    public void setPriceSmall(double priceSmall) {
        this.priceSmall = priceSmall;
    }

    public double getPriceLarge() {
        return priceLarge;
    }

    public void setPriceLarge(double priceLarge) {
        this.priceLarge = priceLarge;
    }

    public List<IngredientDto> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientDto> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "PizzaDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", priceSmall=" + priceSmall +
                ", priceLarge=" + priceLarge +
                ", ingredients=" + ingredients +
                '}';
    }
}
