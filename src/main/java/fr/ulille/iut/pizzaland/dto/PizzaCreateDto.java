package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.stream.Collectors;

public class PizzaCreateDto {
    private String name;
    private double priceLarge;
    private double priceSmall;
    private List<String> ingredients;

    public PizzaCreateDto() {
        ingredients = List.of();
    }

    public PizzaCreateDto(String name, double priceLarge, double priceSmall, List<String> ingredients) {
        this.name = name;
        this.priceLarge = priceLarge;
        this.priceSmall = priceSmall;
        this.ingredients = ingredients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPriceSmall() {
        return priceSmall;
    }

    public void setPriceSmall(double priceSmall) {
        this.priceSmall = priceSmall;
    }

    public double getPriceLarge() {
        return priceLarge;
    }

    public void setPriceLarge(double priceLarge) {
        this.priceLarge = priceLarge;
    }

    public List<IngredientCreateDto> getIngredientCreateDto() {
        return ingredients.parallelStream().map(IngredientCreateDto::new).collect(Collectors.toList());
    }

    public void setIngredientCreateDto(List<IngredientCreateDto> ingredients) {
        this.ingredients = ingredients.parallelStream().map(IngredientCreateDto::getName).collect(Collectors.toList());
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }
}
