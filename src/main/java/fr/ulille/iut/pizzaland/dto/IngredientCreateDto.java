package fr.ulille.iut.pizzaland.dto;

public class IngredientCreateDto {
    private String name;

    public IngredientCreateDto() {}

    public IngredientCreateDto(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

