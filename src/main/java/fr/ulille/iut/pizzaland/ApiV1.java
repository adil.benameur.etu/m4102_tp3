package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import jakarta.json.bind.JsonbBuilder;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
    private static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

    public ApiV1() {
        packages("fr.ulille.iut.pizzaland");

        String environment = System.getenv("PIZZAENV");

        if ( environment != null && environment.equals("withdb") ) {
            LOGGER.info("Loading with database");
            try {
                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
                pizzaDao.dropTableAndIngredientAssociation();

                FileReader ingredientsReader = new FileReader( getClass().getClassLoader().getResource("ingredients.json").getFile() );
                List<Ingredient> ingredients = JsonbBuilder.create().fromJson(ingredientsReader, new ArrayList<Ingredient>(){}.getClass().getGenericSuperclass());

                IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
                ingredientDao.dropTable();
                ingredientDao.createTable();
                for ( Ingredient ingredient: ingredients) {
                    ingredientDao.insert(ingredient);
                }

                FileReader pizzasReader = new FileReader( getClass().getClassLoader().getResource("pizzas.json").getFile() );
                List<Pizza> pizzas = JsonbBuilder.create().fromJson(pizzasReader, new ArrayList<Pizza>(){}.getClass().getGenericSuperclass());

                pizzaDao.createTableAndIngredientAssociation();
                for ( Pizza pizza: pizzas) {
                    pizzaDao.insert(pizza);
                }
            } catch ( Exception ex ) {
                throw new IllegalStateException(ex);
            }
        }
    }
}
