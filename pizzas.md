# Projet REST avec Jersey
### API des pizzas

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| ------------------------ | ----------- | ------------------------------------------------------------ | --------------- | -------------------------------------------------------------------- |
| /pizzas                  | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (I2)                                                |
| /pizzas/{id}             | GET         | <-application/json<br><-application/xml                      |                 | une pizza (I2) ou 404                                                |
| /pizzas                  | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Ingrédient (I1) | Nouvel ingrédient (I2)<br>409 si l'ingrédient existe déjà (même nom) |
| /pizzas/{id}             | DELETE      |                                                              |                 |                                                                      |


Une pizza comporte un identifiant, un nom, un prix small, un prix large, une liste d'ingrédients. Sa
représentation JSON (I2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "Napolitaine",
      "price_small": 5.5,
      "price_large": 7.0,
      "ingredients": [
             {
                 id: "123e4567-e89b-12d3-a456-426614174000", 
                 name: "mozarella"
             },
             {
                 id: "123e4567-e89b-12d3-a456-426614174000", 
                 name: "sauce tomate"
             },
             {
                 id: "123e4567-e89b-12d3-a456-426614174000", 
                 name: "basilic"
             },
             {
                 id: "123e4567-e89b-12d3-a456-426614174000", 
                 name: "olive"
             }
         ]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente un ingredient. Aussi on aura une
représentation JSON (I1) qui comporte uniquement un nom, un prix small, un prix large, une liste d'ingrédients sans ID :

    {       "name": "Napolitaine",
            "price_small": 5.5,
            "price_large": 7.0,
            "ingredients": ["mozarella", "sauce tomate", "basilic", "olive"]
    }
    