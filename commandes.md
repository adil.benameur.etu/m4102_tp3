# Projet REST avec Jersey
### API des commandes

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| ------------------------ | ----------- | ------------------------------------------------------------ | --------------- | -------------------------------------------------------------------- |
| /commandes               | GET         | <-application/json<br><-application/xml                      |                 | liste les commandes (I2)                                             |
| /commandes/{id}          | GET         | <-application/json<br><-application/xml                      |                 | une commande (I2) ou 404                                             |
| /commandes               | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Commandes (I1)  | Nouvel commande (I2)<br>409 si l'ingrédient existe déjà (même nom)   |
| /commandes/{id}          | DELETE      |                                                              |                 |                                                                      |


Une commande comporte un identifiant, un nom, un prénom, un total, une liste d'IDs de pizza. Sa
représentation JSON (I2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "prenom": "Adil",
      "nom": "Benameur",
      "total": 10.0,
      "pizzas": [
            {
                "id": "7de82ed7-6147-415d-865e-b59b1949256d",
                "ingredients": [
                    {
                        "id": "c77deeee-d50d-49d5-9695-c98ec811f762",
                        "name": "tomate"
                    },
                    {
                        "id": "d36903e1-0cc0-4bd6-a0ed-e0e9bf7b4037",
                        "name": "jambon"
                    },
                    {
                        "id": "dee27dd6-f9b6-4d03-ac4b-216b5c9c8bd7",
                        "name": "lardons"
                    },
                    {
                        "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
                        "name": "mozzarella"
                    }
                ],
                "name": "savoyarde",
                "priceLarge": 10.5,
                "priceSmall": 8.5
            }
         ]
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente un ingredient. Aussi on aura une
représentation JSON (I1) qui comporte uniquement un nom, nu prénom, un total, une liste d'IDs de pizza :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "prenom": "Adil",
      "nom": "Benameur",
      "total": 10.0,
      "pizzas": ["7de82ed7-6147-415d-865e-b59b1949256d"]
    }
    